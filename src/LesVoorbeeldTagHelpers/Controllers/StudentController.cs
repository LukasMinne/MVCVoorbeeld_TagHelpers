﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LesVoorbeeldTagHelpers.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldTagHelpers.Controllers
{
    public class StudentController : Controller
    {
        // GET: /<controller>/
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        public IActionResult Add()
        {
            return View(new Student() { Id=100 });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(Student student)
        {
            string naamEinde = "eke";
            if (ModelState.IsValid)
            {
                if (student.Naam.EndsWith(naamEinde))
                {
                    ModelState.AddModelError("StudentNaam", $"De naam mag niet eindigen op '{naamEinde}'");
                    return View(student);
                }
                return View("Finish", student);
            }

            return View(student);
        }

    }
}
